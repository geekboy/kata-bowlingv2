<?php

interface BowlingInterface
{
    public function stringPinesToRolls($stringPins);
    public function isSpare();
    public function isStrike($numOfFrames);
    public function addRolls($pins);
    public function checkBonus($frames);
    public function score();
}

class Bowling implements  BowlingInterface {
    private $rolls = array();
    private $bonuscount=0;
    private $checkSpare=0;
    private $nBalls=0;
    public $arrayBonus=array(0,0,0);


    public function addRolls($pins) {
        $this->rolls[] = $pins;
    }

    public function stringPinesToRolls($stringPins){
        $arryPins = str_split($stringPins);
        $max=sizeof($arryPins);
        for($pins=0; $pins<$max; $pins++) {

            switch (ucfirst($arryPins[$pins])){
                case "X":
                    $this->addRolls(10);
                    break;
                case "/":
                    $this->addRolls(5);
                    $this->addRolls(5);
                    break;
                case "-":
                    $this->addRolls(0);
                    break;
                default:
                    $this->addRolls((int)$arryPins[$pins]);
                    break;
            }
        }
    }

    public function isStrike($frames){
        return $this->rolls[$frames]==10?true:false;
    }

    public function addScore($frames, $max, $nBalls, $m, $n){
        if($frames<($max-$nBalls)) {
            $this->moreBonus($m, $n);
            return 10;
        }else{
            return 0;
        }
    }

    public function isSpare(){
        return $this->checkSpare==10?true:false;
    }

    public function checkBonus($frames){
        $bonus=0;
        for($i=0; $i<$this->arrayBonus[0]; $i++){
            $bonus+=$this->rolls[$frames];
        }
        return $bonus;
    }

    public function moreBonus($n, $x){
        $this->arrayBonus[1]+=$n;
        $this->arrayBonus[2]+=$x;
    }
    public function moveArrayBonus(){
        $this->arrayBonus[0]=$this->arrayBonus[1];
        $this->arrayBonus[1]=$this->arrayBonus[2];
        $this->arrayBonus[2]=0;
    }

    public function lastTwoBalls(){
        if($this->nBalls==2){
            $score=$this->checkSpare;
            $this->checkSpare=0;
            $this->nBalls=0;
            return $score;
        }else{
            return 0;
        }
    }


    public function score(){
        $score=0;
        $max=count($this->rolls);
        for($frames=0; $frames<$max; $frames++)
        {
            $this->moveArrayBonus();
            $score+=$this->checkBonus($frames);
            if($this->isStrike($frames)){
                $score+=$this->addScore($frames, $max, 2, 1, 1);
            }else{
                $this->nBalls++;
                $this->checkSpare+=$this->rolls[$frames];
                if ($this->isSpare()) {
                    $score+=$this->addScore($frames, $max, 1, 1, 0);
                    $this->checkSpare=0;
                    $this->nBalls=0;
                }
                $score+=$this->lastTwoBalls();
            }
        }
        return $score;
    }
}

