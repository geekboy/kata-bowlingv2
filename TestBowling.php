<?php

require_once("bowling.php");

class TestBowling extends PHPUnit_Framework_TestCase {

    protected $game;
    protected function setUp(){
        $this->game=new Bowling();
    }

    public function simulatedFramesPines($frames, $pins){
        for($i=0; $i<$frames; $i++){
            $this->game->addRolls($pins);
        }
    }

    public function test0pines(){
        $this->simulatedFramesPines(20, 0);
        $this->assertEquals(0, $this->game->score());
    }

    public function test2pines(){
        $this->simulatedFramesPines(20, 2);
        $this->assertEquals(40, $this->game->score());
    }

    public function test2pinesAndRollof5(){
        $this->game->addRolls(5);
        $this->simulatedFramesPines(19, 2);
        $this->assertEquals(43, $this->game->score());
    }

    public function testOneSpare(){
       $this->game->addRolls(5);
       $this->game->addRolls(5);
       $this->game->addRolls(3);
       $this->simulatedFramesPines(17, 0);
       $this->assertEquals(16, $this->game->score());
    }

   public function testOneSpareDiferentRolls(){
       $this->game->addRolls(7);
       $this->game->addRolls(3);
       $this->game->addRolls(3);
       $this->simulatedFramesPines(17, 0);
       $this->assertEquals(16, $this->game->score());
   }

   public function testOneStrike(){
       $this->game->addRolls(10);
       $this->game->addRolls(3);
       $this->game->addRolls(5);
       $this->simulatedFramesPines(16, 0);
       $this->assertEquals(26, $this->game->score());
   }

    public function testOneStrikeDifRolls(){
        $this->game->addRolls(10);
        $this->game->addRolls(2);
        $this->game->addRolls(2);
        $this->simulatedFramesPines(16, 0);
        $this->assertEquals(18, $this->game->score());
    }

   public function testOneStrikeOneSpare(){
       $this->game->addRolls(10);
       $this->game->addRolls(3);
       $this->game->addRolls(5);
       $this->game->addRolls(5);
       $this->game->addRolls(3);
       $this->simulatedFramesPines(14, 0);
       $this->assertEquals(34, $this->game->score());
   }

    public function testStringAllStrikePines(){
        $this->game->stringPinesToRolls("xxxxxxxxxxxx");
        $this->assertEquals(300, $this->game->score());
    }

    public function testStringAllStrikePines2(){
        $this->game->stringPinesToRolls("xxxxxxxxx34");
        $this->assertEquals(257, $this->game->score());
    }

    public function testStringAllStrike(){
        $this->simulatedFramesPines(12, 10);
        $this->assertEquals(300, $this->game->score());
    }

    public function testAllSpare()
    {
        $this->game->stringPinesToRolls("//////////5");
        $this->assertEquals(150, $this->game->score());
    }

}